# Api-Rest: Veterinaria

## Especificación
1. Base Postgres: 
		1.1 ***"Crear base de datos: veterinariadb"***
		1.2 ***"Modificar parámetros en application.properties"***
1. mvn clean package
3. mvn spring-boot:run 
		1.1 ***"Esto creará una instancia donde poder probar la app en el puerto por defecto 8080 ej: localhost:8080/veterinarias"***	
4. Mayor información de la Api en [docs-Api][1]	

[1]: https://localhost:8080/swagger-ui.html
