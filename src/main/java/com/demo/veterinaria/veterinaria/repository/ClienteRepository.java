package com.demo.veterinaria.veterinaria.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.veterinaria.veterinaria.model.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	Page<Cliente> findByVeterinariaId(Long veterinariaId,Pageable pageable);
	Cliente findByNombre(String nombre);
}
