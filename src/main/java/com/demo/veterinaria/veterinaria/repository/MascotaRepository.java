package com.demo.veterinaria.veterinaria.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.veterinaria.veterinaria.model.Mascota;

@Repository
public interface MascotaRepository extends JpaRepository<Mascota, Long> {
	Page<Mascota> findByClienteId(Long clienteId,Pageable pageable);
}
