package com.demo.veterinaria.veterinaria.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.veterinaria.veterinaria.model.Veterinaria;

@Repository
public interface VeterinariaRepository extends JpaRepository<Veterinaria, Long> {
	Page<Veterinaria> findById(Long veterinariaId,Pageable pageable);
}
