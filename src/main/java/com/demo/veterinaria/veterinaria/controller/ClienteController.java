package com.demo.veterinaria.veterinaria.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.veterinaria.veterinaria.exception.ResourceNotFoundException;
import com.demo.veterinaria.veterinaria.model.Cliente;
import com.demo.veterinaria.veterinaria.repository.ClienteRepository;
import com.demo.veterinaria.veterinaria.repository.VeterinariaRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/cliente", description = "Cliente Controller", produces = "application/json")
public class ClienteController {
	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private VeterinariaRepository veterinariaRepository;

	@ApiOperation(value = "get client", response = Cliente.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Detalle de los clientes retornados", response = Cliente.class),
			@ApiResponse(code = 500, message = "Internal Server Error"),
			@ApiResponse(code = 404, message = "Cliente no encontrado") })

	@GetMapping("/clientes")
	public Page<Cliente> getClientes(Pageable pageable) {
		return clienteRepository.findAll(pageable);
	}

	@ApiOperation(value = "Busca los Clientes asociados a una Veterinaria", notes = "Retorna una lista de clientes asociados" )
	@GetMapping("/veterinarias/{veterinariaId}/clientes")
	public Page<Cliente> getClientesByVeterinaria(@PathVariable Long veterinariaId, Pageable pageable) {
		if (!veterinariaRepository.existsById(veterinariaId)) {
			throw new ResourceNotFoundException("Veterinaria not found with id " + veterinariaId);
		}
		return clienteRepository.findByVeterinariaId(veterinariaId, pageable);
	}

	@PostMapping("/clientes")
	public Cliente createCliente(@Valid @RequestBody Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	@ApiOperation(value = "Setea los Clientes asociados a una Veterinaria", notes = "Retorna el cliente seteado" )
	@PostMapping("/veterinarias/{veterinariaId}/clientes")
	public Cliente addCliente(@PathVariable Long veterinariaId, @Valid @RequestBody Cliente cliente) {
		return veterinariaRepository.findById(veterinariaId).map(veterinaria -> {
			cliente.setVeterinaria(veterinaria);
			return clienteRepository.save(cliente);
		}).orElseThrow(() -> new ResourceNotFoundException("Veterinaria not found with id " + veterinariaId));
	}

	/*@PutMapping("/veterinarias/{veterinariaId}/clientes/{clienteId}")
	public Cliente updateCliente(@PathVariable Long veterinariaId, @PathVariable Long clienteId,
			@Valid @RequestBody Cliente clienteRequest) {
		if (!veterinariaRepository.existsById(veterinariaId)) {
			throw new ResourceNotFoundException("Veterinaria not found with id " + veterinariaId);
		}

		return clienteRepository.findById(clienteId).map(cliente -> {
			cliente.setNombre(clienteRequest.getNombre());
			return clienteRepository.save(cliente);
		}).orElseThrow(() -> new ResourceNotFoundException("Cliente not found with id " + clienteId));
	}*/

}
