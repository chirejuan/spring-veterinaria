package com.demo.veterinaria.veterinaria.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.veterinaria.veterinaria.model.Veterinaria;
import com.demo.veterinaria.veterinaria.repository.VeterinariaRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "/veterinaria", description = "Veterinaria Controller", produces = "application/json")
public class VeterinariaController {
	@Autowired
	private VeterinariaRepository veterinariaRepository;
	
	@ApiOperation(value = "Busca todas las Veterinarias", notes = "Retorna una lista de veterinarias" )
	@GetMapping("/veterinarias")
	public Page<Veterinaria> getVeterinaria(Pageable pageable) {
		return veterinariaRepository.findAll(pageable);
	}
	
	@PostMapping("/veterinarias")
	public Veterinaria createVeterinaria(@Valid @RequestBody Veterinaria veterinaria) {
		return veterinariaRepository.save(veterinaria);
	}
	
	@GetMapping("/veterinarias/{veterinariaId}")
    public Optional<Veterinaria> getVeterinariaById(@PathVariable Long veterinariaId) {
        return veterinariaRepository.findById(veterinariaId);
    }
}
