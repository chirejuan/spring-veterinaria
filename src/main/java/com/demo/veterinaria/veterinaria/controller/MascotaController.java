package com.demo.veterinaria.veterinaria.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.veterinaria.veterinaria.exception.ResourceNotFoundException;
import com.demo.veterinaria.veterinaria.model.Mascota;
import com.demo.veterinaria.veterinaria.repository.ClienteRepository;
import com.demo.veterinaria.veterinaria.repository.MascotaRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "/mascota", description = "Mascota Controller", produces = "application/json")
public class MascotaController {
	@Autowired
    private MascotaRepository mascotaRepository;

    @Autowired
    private ClienteRepository clienteRepository;
    
    @ApiOperation(value = "Busca las Mascotas asociadas a un Cliente", notes = "Retorna una lista de mascotas" )
    @GetMapping("/clientes/{clienteId}/mascotas")
	public Page<Mascota> getMascotasByCliente(@PathVariable Long clienteId, Pageable pageable) {
    	if (!clienteRepository.existsById(clienteId)) {
			throw new ResourceNotFoundException("Cliente not found with id " + clienteId);
		}
		return mascotaRepository.findByClienteId(clienteId,pageable);
	}
    
    @ApiOperation(value = "Setea las Mascotas asociadas a una Veterinaria", notes = "Retorna la mascota seteada" )
    @PostMapping("/clientes/{clienteId}/mascotas")
    public Mascota addAnswer(@PathVariable Long clienteId,
                            @Valid @RequestBody Mascota mascota) {
        return clienteRepository.findById(clienteId)
                .map(cliente -> {
                    mascota.setCliente(cliente);
                    return mascotaRepository.save(mascota);
                }).orElseThrow(() -> new ResourceNotFoundException("Cliente not found with id " + clienteId));
    }
}
